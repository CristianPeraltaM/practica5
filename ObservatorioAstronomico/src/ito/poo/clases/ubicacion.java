package ito.poo.clases;

import java.time.LocalDate;

public class ubicacion {
   
   private String Nombre;
   private float Longitud;
   private float Latitud;
   private LocalDate Periodo;
   private float Distancia;
   
   
  

public ubicacion(String Nombre,float Longitud, float Latitud, LocalDate Periodo, float Distancia ) {
		super();
		this.Nombre = Nombre;
		this.Longitud = Longitud;
		this.Latitud = Latitud;
		this.Periodo = Periodo;
		this.Distancia = Distancia;
		
	}

 public String getNombre() {
	return Nombre;
}

public void setNombre(String Nombre) {
	this.Nombre = Nombre;
}

public float getLongitud() {
	return Longitud;
}

public void setLongitud(float Longitud) {
	this.Longitud = Longitud;
}

public float getLatitud() {
	return Latitud;
}

public void setLatitud(float Latitud) {
	this.Latitud = Latitud;
}

public LocalDate getPeriodo() {
	return Periodo;
}

public void setPeriodo(LocalDate Periodo) {
	this.Periodo = Periodo;
}

public float getDistancia() {
	return Distancia;
}

public void setDistancia(float Distancia) {
	this.Distancia = Distancia;
}

@Override
public String toString() {
	return "ubicacion [Nombre=" + Nombre + ", Longitud=" + Longitud + ", Latitud=" + Latitud + ", Periodo=" + Periodo
			+ ", Distancia=" + Distancia + "]";
}




}
  