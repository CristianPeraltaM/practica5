package ito.poo.clases;
    import java.util.ArrayList;

public class CuerpoCeleste {
	
	 private String Composicion;
	 private ArrayList<ubicacion>ubicaciones;
	 
	 
	
	public CuerpoCeleste( String Composicion) {
			super();
			
			this.Composicion = Composicion;
		    ubicaciones= new ArrayList<ubicacion>();
		}

    public void agregarubicacion(ubicacion u) {
    	if(!this.ubicaciones.contains(u)) {
    		this.ubicaciones.add(u);
    	}
    }

    public int obtieneDesplazamiento(int i, int j) {
    	int distancia=-1;
    	if(i<this.ubicaciones.size() && j<this.ubicaciones.size()) {
    		ubicacion ui,uj;
    		ui= this.ubicaciones.get(i);
    		uj= this.ubicaciones.get(j);
    		distancia = (int) Math.abs(ui.getDistancia()-uj.getDistancia());
    	}
    		
    	return distancia;
    }
    
    
	public String getComposicion() {
		return Composicion;
	}



	public void setComposicion(String Composicion) {
		this.Composicion = Composicion;
	}



	public ArrayList<ubicacion> getUbicaciones() {
		return ubicaciones;
	}



	@Override
	public String toString() {
		return "CuerpoCeleste [ Composicion=" + Composicion + ", ubicaciones=" + ubicaciones
				+ "]";
	}
	
}
	
		
	 

